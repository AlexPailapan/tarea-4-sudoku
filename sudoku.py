#Python 3.10.0  Windows 10
#IDLE Python

#Tabla Sudoku (s)
s= [[5,3,0, 0,7,0, 0,0,0],
    [6,0,0, 1,9,5, 0,0,0],
    [0,9,8, 0,0,0, 0,6,0],
     
    [8,0,0, 0,6,0, 0,0,3],
    [4,0,0, 8,0,3, 0,0,1],
    [7,0,0, 0,2,0, 0,0,6],
     
    [0,6,0, 0,0,0, 2,8,0],
    [0,0,0, 4,1,9, 0,0,5],
    [0,0,0, 0,8,0, 0,7,9]]

#Muestra en pantalla el Sudoku original, separando las regiones
print('Sudoku original')
for a in range(0,9): #Indicador para las filas
    for b in range(0,9): #Indicador para la columnas
        print(s[a][b],end=' ') #Imprime sin salto de linea
        if b==2 or b==5:        #En el indice 2 y 5 agrega un espacio en las columnas
            print(' ',end=' ')
        elif b==8:              #Print en una nueva linea
            print('')       
        if (a==2 or a==5) and b==8: #Imprime un salto al final de las filas 2 y 5
            print('')

print('\n')

    
#Función que busca en las filas, columnas y regiones números en el rango 1-9
#y devuelve el valor lógico False si lo encuentra y True si no está.
#Permite que se ingrese la posición de la casilla: fila, columna y el valor.
def busqueda(fila,columna,numero):
    for i in range(0,9):
        if s[fila][i]==numero: #Busqueda del numero en la fila de la casilla ubicada en (fila, columna)
            return False        #Si lo encuentra entrega el valor lógico False
        if s[i][columna]==numero: #Busqueda del numero en la columna de la casilla ubicada en (fila, columna)
            return False            #Si lo encuentra entrega el valor lógico False

    fila_region=(fila//3)*3 #Define la fila de inicio de la región a la que pertenece la casilla
    columna_region=(columna//3)*3 #Define la columna de inicio de la región a la que pertenece la casilla
    for i in range(0,3):     #   
        for j in range(0,3): #Recorre la region de la casilla
            if s[fila_region+i][columna_region+j]==numero: #Si encuentra el número en la región entrega el valor lógico False
                return False   
    return True #Si el valor no está en la region, fila y columna, entrega el valor lógico True


#Busca las casillas vacías (las que tienen un número 0)
def asignacion():
    for x in range(0,9):
        for y in range(0,9):            #Recorre todas las casilla (9x9=81)
            if s[x][y]==0:              #Compara el valor de la casilla con 0
                for z in range(1,10):   #Prueba los números del 1 al 9
                    if busqueda(x,y,z): #Ingresa cada valor a la función busqueda() para saber la posibilidad de asignarlo en la casilla
                        s[x][y]=z       #Si es posible lo asigna (El valor lógico devuelto desde la función busqueda es True)
                        asignacion()    #Accion recursiva al usar nuevamente la funcion definida dentro de ella misma
                        s[x][y]=0       #Si no hay posibilidad en la funcion "busqueda()", vuelve a la anterior y reasigna un 0 para probar otro numero posible
                return
            
    for a in range(0,9):                #Muestra en pantalla la matriz separada por regiones, igual que en la impresión del Sudoku inicial.
        for b in range(0,9):            #
            print(s[a][b],end=' ')      #
            if b==2 or b==5:            #
                print(' ',end=' ')      #
            elif b==8:                  #
                print('')               #
            if (a==2 or a==5) and b==8: #
                print('')               #
print('\n')            
print('Sudoku resuelto') #Título del Sudoku resuelto

asignacion()#Ejecuta la resolución del Sudoku (s) llamando a la función asignación()
